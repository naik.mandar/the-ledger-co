package com.oorjaa.ledger.repository;

import com.oorjaa.ledger.entities.LoanRecord;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class LoanRepository {
    List<LoanRecord> loanRecordList = new ArrayList<>();

    public Boolean addLoanRecordToList(LoanRecord loanRecord) {
        if(loanRecord != null) {
            return loanRecordList.add(loanRecord);
        } else {
            return false;
        }
    }
}
