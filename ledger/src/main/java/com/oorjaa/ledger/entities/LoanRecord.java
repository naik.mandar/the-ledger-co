package com.oorjaa.ledger.entities;

public class LoanRecord {
    String bankName;
    String borrowerName;
    int principalAmount;
    int noOfYears;
    int rateOfInterest;

    public LoanRecord(String bankName, String borrowerName, int principalAmount, int noOfYears, int rateOfInterest) {
        this.bankName = bankName;
        this.borrowerName = borrowerName;
        this.principalAmount = principalAmount;
        this.noOfYears = noOfYears;
        this.rateOfInterest = rateOfInterest;
    }

    public String getBankName() {
        return bankName;
    }

    public String getBorrowerName() {
        return borrowerName;
    }

    public int getPrincipalAmount() {
        return principalAmount;
    }

    public int getNoOfYears() {
        return noOfYears;
    }

    public int getRateOfInterest() {
        return rateOfInterest;
    }
}
