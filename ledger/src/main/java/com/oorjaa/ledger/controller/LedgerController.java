package com.oorjaa.ledger.controller;

import com.oorjaa.ledger.entities.LoanRecord;
import com.oorjaa.ledger.service.LoanService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class LedgerController {

//    @Autowired
    private LoanService loanService = new LoanService();

    @PostMapping(value = "/loan")
    public ResponseEntity<LoanRecord> loan(String bankName, String borrowerName, int principleAmount, int noOfYears, int rateOfInterest) {
         return loanService.createLoanRecord(bankName, borrowerName, principleAmount, noOfYears, rateOfInterest);
    }
}
