package com.oorjaa.ledger.service;

import com.oorjaa.ledger.entities.LoanRecord;
import com.oorjaa.ledger.repository.LoanRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.net.URI;

@Service
public class LoanService {

//    @Autowired
    LoanRepository loanRepository = new LoanRepository();

    public ResponseEntity<LoanRecord> createLoanRecord(String bankName, String borrowerName, int principleAmount, int noOfYears, int rateOfInterest) {
        LoanRecord loanRecord = new LoanRecord(bankName, borrowerName, principleAmount, noOfYears, rateOfInterest);
        if(loanRepository.addLoanRecordToList(loanRecord)) {
            return ResponseEntity.created(URI.create("/loan")).body(loanRecord);
        } else {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }
}

