package com.oorjaa.ledger;

import com.oorjaa.ledger.controller.LedgerController;
import com.oorjaa.ledger.entities.LoanRecord;
import com.oorjaa.ledger.service.LoanService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import static org.mockito.Mockito.mock;

class ControllerTests {
    private static LedgerController ledger;

    private static LoanService loanService;

    @BeforeAll
    public static void setUp() {
        ledger = new LedgerController();
        loanService = mock(LoanService.class);
    }

    @Test
    public void testLoanCommandForHttpsStatusCreated() {

        Mockito.when(loanService.createLoanRecord("IDIDI", "Dale", 10000, 5, 4))
                .thenReturn(ResponseEntity.status(HttpStatus.CREATED).build());

        ResponseEntity<LoanRecord> actual = ledger.loan("IDIDI", "Dale", 10000, 5, 4);

        Assertions.assertEquals(HttpStatus.CREATED, actual.getStatusCode());
        Assertions.assertEquals("IDIDI", actual.getBody().getBankName());
        Assertions.assertEquals("Dale", actual.getBody().getBorrowerName());
        Assertions.assertEquals(10000, actual.getBody().getPrincipalAmount());
        Assertions.assertEquals(5, actual.getBody().getNoOfYears());
        Assertions.assertEquals(4, actual.getBody().getRateOfInterest());

    }

    @Test
    public void testLoanCommandForHttpsStatusInternalServerError() {

        Mockito.when(loanService.createLoanRecord("IDIDI", "Dale", 10000, 5, 4))
                .thenReturn(ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build());

        ResponseEntity<LoanRecord> actual = ledger.loan("IDIDI", "Dale", 10000, 5, 4);

        Assertions.assertEquals(HttpStatus.CREATED, actual.getStatusCode());
        Assertions.assertEquals("IDIDI", actual.getBody().getBankName());
        Assertions.assertEquals("Dale", actual.getBody().getBorrowerName());
        Assertions.assertEquals(10000, actual.getBody().getPrincipalAmount());
        Assertions.assertEquals(5, actual.getBody().getNoOfYears());
        Assertions.assertEquals(4, actual.getBody().getRateOfInterest());

    }

    public void testBalanceCommand() {
        
    }
}